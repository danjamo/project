<div class="navbar navbar-expand-md navbar-light bg-light border-bottom welcome-nav">
    <div class="navbar-brand" style="width: 40%; max-width: 200px;">
        <a href="/">
            <img src="{{asset('img/logo.png')}}" alt="" class="img-fluid">
        </a>
    </div>
    <button class="navbar-toggler" type="button"  data-toggle="collapse" data-target="#indexNav" aria-controls="indexNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="indexNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                    <a href="/posts" class="nav-link h6">Test</a>
            </li>
            @if (strpos($page, 'about') !== false) 
                <li class="nav-item active">
                    <a href="/about" class="nav-link h6">Who we are</a>
                </li>
            @else
                <li class="nav-item">
                    <a href="/about" class="nav-link h6">Who we are</a>
                </li>
            @endif
            @if (strpos($page, 'terms') !== false) 
                <li class="nav-item active">
                    <a href="/terms" class="nav-link h6">Terms and Conditions</a>
                </li>
            @else
                <li class="nav-item">
                    <a href="/terms" class="nav-link h6">Terms and Conditions</a>
                </li>
            @endif

            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link h6" href="{{ route('login') }}">{{ __('Sign In') }}</a>
                </li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>  
</div>