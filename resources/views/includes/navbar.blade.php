@guest
    <div class="navbar navbar-expand-md navbar-light bg-light  welcome-nav border-bottom">
        <div class="navbar-brand" style="width: 40%; max-width: 200px;">
            <a href="/">
                <img src="{{ asset('img/logo.png') }}" alt="" class="img-fluid">
            </a>
        </div>
        <button class="navbar-toggler" type="button"  data-toggle="collapse" data-target="#indexNav" aria-controls="indexNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="indexNav">
            <ul class="navbar-nav ml-auto">
                <a href="/" class="nav-link">Home</a>
                <a href="/login" class="nav-link">Sign In</a>
                <a href="/about" class="nav-link">Who we are</a>
                <a href="/terms" class="nav-link">Terms and Conditions</a>
            </ul>
        </div>
    </div>
@else
<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#" style="width: 40%; max-width: 200px;">
      <img src="{{ asset('img/logo.png') }}" class="img-fluid" alt="logo">
    </a>

      <div class="current-user-container ml-auto">
        <div class="dropdown">
            <a class="h6 text-decoration-none text-capitalize dropdown-toggle" href="#" role="button" id="currentUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->lname }}, {{ Auth::user()->fname }} 
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="currentUser">
                <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
            </div>
        </div>
    </div>
</nav>

<nav class="navbar navbar-dark bg-dark border-bottom navbar-expand-xl">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuNav" aria-controls="menuNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
      <div class="collapse navbar-collapse" id="menuNav">
        <ul class="navbar-nav m-auto">
              <li class="nav-item px-3">
                   <a class="nav-link" href="#">Transactions</a>
              </li>
              <li class="nav-item px-3">
                  <a class="nav-link" href="/admin/create">
                      <!-- <span class="sr-only">(current)</span> -->
                  Add Member</a>
              </li>
              <li class="nav-item px-3">
              <a class="nav-link" href="/admin">Manage Accounts</a>
              </li>
              <li class="nav-item px-3">
                  <a class="nav-link" href="#">Requests <span class="badge badge-pill badge-danger">1</span></a>
              </li>
              <li class="nav-item px-3">
                  <a class="nav-link" href="#">Reports</a>
              </li>
              <li class="nav-item px-3">
                  <a class="nav-link" href="#">Receipts</a>
              </li>
        </ul>
      </div>
</nav>

@endguest