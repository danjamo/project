@extends('layouts.welcome')

@section('title')
<title>Sign in to Alkansya</title>
@endsection

@section('content')

@if (Auth::check())
<script>window.location = 'admin/Add_Member';</script>
@endif

<div class="main-login">
    <div class="main-login-container mx-auto">
        <div class="container">
            <div class="d-flex flex-column align-items-center">
                <div class="login-logo m-3">
                    <a href="/">
                        <img src="{{asset('img/logo.png')}}" alt="" class="img-fluid">
                    </a>
                </div>
                <h5 class="login-title">Sign in</h5>

                {{-- If the login is invalid --}}
                 @if ($message = Session::get('error'))
                    <div class="text-center pt-2">
                        <span class="text-danger text-center"><strong>{{ $message }}</strong></span><br>
                        <small class="text-danger">Incorrect Member ID or Password</small>
                    </div>
                @endif

                {{-- @if ($message = Session::get('login'))
                    <p class="text-danger text-center"><strong>{{ $message }}</strong></p>
                @endif      


                @if ( count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li> {{ $error }} </li>
                            @endforeach
                        </ul>
                    </div>
                @endif --}}

                <div class="login-form-container w-100 clearfix">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
							<small>Member ID</small>
                        <input name="id" type="text" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{ old('id') }}" autofocus>

                        @if ( $errors->has('id') )
                            <div class="invalid-feedback">{{ $errors->first('id') }}</div>
                        @endif

                        </div>
                        <div class="form-group">
                            <small>Password</small>
                            <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ isset($message) ? '' : old('password') }}" autofocus>
                            @if ( $errors->has('password') )
                                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                        <div class="pt-3 cleafix">
                            <a href="">Forgot Password?</a>
                            <button type="submit" class="btn btn-primary float-right px-4">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection