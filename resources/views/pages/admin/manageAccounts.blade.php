@extends('layouts.app')

@section('title')
<title>Welcome to Alkansya App</title>
@endsection

@section('content')
<h3>Manage Accounts</h3>
<div class="row">
    <div class="col">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Member ID</th>
                    <th>Complete Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                @if ( count($users) > 0 )
                    @foreach ($users as $user)
                        @if ( $user->user_type != 2 )
                            <tr class="manage-accounts clickable-row" data-href="admin/{{ $user->id }}">
                            <td>{{ $user->id }}</td>
                            <td class="text-capitalize">
                                <?php
                                echo $user->lname.", ".$user->fname." ".$user->mname;
                                ?>
                            </td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if ( $user->user_type == 1 )
                                    Collector
                                @else
                                    Member
                                @endif
                            </td>
                        </tr>
                        @endif
                    @endforeach
                @else
                <tr>
                    <td colspan="4" class="text-center">No Entries Found</td>
                </tr>
                @endif
            </tbody>
        </table>
        <div class="d-flex justify-content-center">
            {{ $users->links() }}
        </div>
    </div>
</div>
@endsection