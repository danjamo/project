@extends('layouts.app')

@section('title')
<title>{{ $user->id }} - {{ $user->lname }},  {{ $user->fname }} {{ $user->mname }}}</title>
@endsection

@section('content')
<h3>Manage Accounts > View User Information</h3>
<div class="row">
    <div class="col-sm col-md-10 col-xl-8">
        <div class="py-3">
            <a class="btn btn-light" role="button" href="/admin">&lsaquo; Go back to Manage Accounts Table</a>
        </div>
        <div class="card">
            <div class="card-header">
                User information
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">Member ID</div>
                        <div class="col font-weight-bold">{{ $user->id }}</div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">Last Name</div>
                        <div class="col font-weight-bold text-capitalize">{{ $user->lname }}</div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">First Name</div>
                        <div class="col font-weight-bold text-capitalize">{{ $user->fname }}</div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">Middle Name</div>
                        <div class="col font-weight-bold text-capitalize">{{ $user->mname }}</div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">Email</div>
                        <div class="col font-weight-bold">{{ $user->email }}</div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">Role</div>
                        <div class="col font-weight-bold text-capitalize">
                            @if ( $user->user_type == 2 )
                                Admin
                            @elseif ( $user->user_type == 1 )
                                Collector
                            @else
                                Member
                            @endif
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">Date Created</div>
                        <div class="col font-weight-bold">{{ $user->created_at }}</div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row mt-3">
            <div class="col">
                <button class="btn btn-outline-warning btn-block" type="button">Archive</button>
            </div>
            <div class="col">
                    <button class="btn btn-success btn-block edit-button" type="button">Edit</button>
            </div>
        </div>
    </div>
</div>
@endsection