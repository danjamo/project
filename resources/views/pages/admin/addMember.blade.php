@extends('layouts.app')

@section('title')
<title>Welcome to Alkansya App</title>
@endsection

@section('content')

@if (Auth::check())
@else
    <script>window.location = "/login";</script>
@endif

{{-- Gets the last member ID and increments it
or creates a new one depending on the year --}}
@if ( count($users) > 0 )
    @foreach ($users as $user)
        <?php 
            // Get the last ID number
            $last_id = $user->id;

            $get_year = now()->year - 2000;
            $result = floor($last_id/10000);
            // result in 2 digit number

            // echo $result."<br>"; 
            
            if ( $result == $get_year ) {
                // echo "true";
                $ctr = $result * 10000;
                $result = ($get_year*10000)+(($last_id - $ctr)+1);

            } else {
                // echo "false";
                $result = $get_year*10000;
                $result++;
            }
            // echo "<br>".$result;

            // $get_year = now()->year - 2000;

            // // Assuming...
            // $last_id = 1;

            // $result = floor($last_id/10000);
            // // result in 2 digit number

            // echo $result."<br>"; 

            // if ( $result == $get_year ) {
            //     echo "true";
            //     $ctr = $result * 10000;
            //     $result = ($get_year*10000)+(($last_id - $ctr)+1);

            // } else {
            //     echo "false";
            //     $result = $get_year*10000;
            //     $result++;
            // }
            // echo "<br>".$result;
        ?>
    @endforeach
@endif

<h3>Add Member</h3>
<div class="row">
    <div class="col-sm-10 col-md-7 col-lg-5 my-3">  
        {{-- <form action=" " method="POST">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="addMemberId">Member ID</label>
                            <input type="text" class="form-control" id="addMemberId" aria-describedby="memberIdNote" value="{{ $result }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="addMemberType">Account Type</label>
                        <select id="addMemberType" class="form-control">
                            <option value="1" selected>Member</option>
                            <option value="2">Collector</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="addMemberLastName">Last Name</label>
                        <input type="text" class="form-control" id="addMemberLastName" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="addMemberFirstName">First Name</label>
                        <input type="text" class="form-control" id="addMemberFirstName" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="addMemberMiddleName">Middle Name</label>
                        <input type="text" class="form-control" id="addMemberMiddleName" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="addMemberEmail">Email</label>
                        <input type="text" class="form-control" id="addMemberEmail" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="addMemberAddress">Complete Address</label>
                        <textarea class="form-control" id="addMemberAddress" rows="2" placeholder="Street number, Barangay, City/Town, Province, Philippines, Zip Code"></textarea>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary my-1">Submit</button>
        </form> --}}
        {!! Form::open(['action' => 'UsersController@store', 'method' => 'POST']) !!}
            <div class="form-group">
                {{ Form::label('id', 'Member ID') }}
                {{ Form::text('id', $result, ['class' => 'form-control', 'readonly']) }}
            </div>
            <div class="form-group">
                {{ Form::label('type', 'Account Type') }}
                {{ Form::select('type', [0 => 'Member', 1 => 'Collector'], 0, ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('lname', 'Last Name') }}
                {{ Form::text('lname', '', ['class' => $errors->has('lname') ? 'form-control is-invalid' : 'form-control']) }}
                @if ($errors->has('lname'))
                    <div class="invalid-feedback">{{ $errors->first('lname') }}</div>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('fname', 'First Name') }}
                {{ Form::text('fname', '', ['class' => $errors->has('fname') ? 'form-control is-invalid' : 'form-control']) }}
                @if ($errors->has('fname'))
                    <div class="invalid-feedback">{{ $errors->first('fname') }}</div>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('mname', 'Middle Name') }}
                {{ Form::text('mname', '', ['class' => $errors->has('mname') ? 'form-control is-invalid' : 'form-control']) }}
                @if ($errors->has('mname'))
                    <div class="invalid-feedback">{{ $errors->first('mname') }}</div>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('email', 'Email') }}
                {{ Form::email('email', '', ['class' => $errors->has('email') ? 'form-control is-invalid' : 'form-control']) }}
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('address', 'Complete Address') }}
                {{ Form::textarea('address', '', ['class' => $errors->has('address') ? 'form-control is-invalid' : 'form-control', 'rows' => 2, 'placeholder' => 'Street number, Barangay, City/Town, Province, Philippines, Zip Code']) }}
                @if ($errors->has('address'))
                    <div class="invalid-feedback">{{ $errors->first('address') }}</div>
                @endif
            </div>
            {{ Form::submit('Create Member', ['class' => 'btn btn-primary']) }}
        {!! Form::close() !!}
    </div>
</div>
@endsection