<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		
		@if (Auth::check())
		@else
			<script>window.location = "/login";</script>
		@endif

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
	
		@yield('title')
	
		<!-- Styles -->
		<link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body>
		@include('includes.navbar')
		<main class="main-content py-3">
			<div class="container">
				@include('includes.messages')
				@yield('content')
			</div>
		</main>
		
		@include('includes.footer')
		@include('includes.scripts')
	</body>
</html>