@guest
<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		@yield('title')
	
		<!-- Styles -->
		<link rel="stylesheet" href="{{ asset('css/app.css')}}">
    </head>
    <body>
		@include('includes.navbar')
		@yield('content')
		@include('includes.footer')
	</body>
	@include('includes.scripts')
</html>	
@else
@if (Auth::check())
<script>window.location = 'admin/Add_Member';</script>
@endif
@endguest