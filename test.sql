-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2019 at 08:59 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `address` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `lname`, `fname`, `mname`, `user_type`, `address`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'I', 'Am', 'Joker', 2, 'jugan', 'joker@email.com', NULL, '$2y$10$RBWW8QpMy.yOAJ2Ya5ismOgQTw42VNa80hG00fTl5Pfu/2swn/l2G', '1JtLXHMSaxK39dA1lL6gxUdR8VEceQvsYk1tDKYfTATT7ky0mFtkiMOkFRXQ', '2019-01-05 03:40:23', '2019-01-06 10:24:40'),
(190001, 'gray', 'cat', 'binaliktad', 0, 'meow, meow street', 'cat@email.com', NULL, '$2y$10$JcPfcMXATuuhxHcQGYJnvemOzBvM9ijcvO9KtFkrj.Hj3RJ1Ub3GC', NULL, '2019-01-06 10:47:47', '2019-01-06 10:47:47'),
(190002, 'meng', 'gay', 'kitty', 0, 'lezgo', 'meow@email.com', NULL, '$2y$10$V90UCa.63Xkbruk6H18sQOBbIvlkvdmSYswOqARglvaEAVl7ANoRe', NULL, '2019-01-06 10:59:09', '2019-01-06 10:59:09'),
(190003, 'Escariote', 'Judas', 'Mah Friend', 1, 'somewhere down there', 'judas@email.com', NULL, '$2y$10$0jq1hVfuYMYsUam4pTuKzeQnClek4KDQoZPNxQtgnMyQJ41//qMKe', NULL, '2019-01-06 11:14:01', '2019-01-06 11:14:01'),
(190004, 'World Ender', 'Thanos', 'Nos', 1, 'banaue rice resort and spa', 'thanos@email.com', NULL, '$2y$10$4I4gG/LG/LrmrtD1UbgGw.pQEtzYJMMLvdri99.mMLpBoC.Hx7.BC', NULL, '2019-01-06 11:17:03', '2019-01-06 11:17:03'),
(190005, 'acho', 'acho', 'acho', 0, 'acho', 'acho@email.com', NULL, '$2y$10$6u8ydq7xpUEneb16esm/S.WEOk.imHYdmFavq8SkQ7GhlpDyadlEq', NULL, '2019-01-06 11:18:18', '2019-01-06 11:18:18'),
(190006, 'hey', 'hey', 'you you', 0, 'i dont like your girlfriend', 'hey@email.com', NULL, '$2y$10$KJ6Zox8ZyV9QXHJiG/E2YufYBn5w.BAqbAz7pQ4Hn2Ivl/eleRRXq', NULL, '2019-01-06 11:26:07', '2019-01-06 11:26:07'),
(190007, 'bugbug', 'o', 'dignidad', 0, 'hahahahaha', 'bully@email.com', NULL, '$2y$10$qWUtOo0FL4Uzt.C4Mq0FzOyQ8dk7tj4Q73/iv3Y5E8pcJRpHCcbU.', NULL, '2019-01-06 11:29:00', '2019-01-06 11:29:00'),
(190008, 'paano', 'mo', 'nasabe', 0, 'ipa drugtest nyo nga ako para malaman nyo', 'meme@email.com', NULL, '$2y$10$fiugnpLUuJRTp1Av1d0dCubp4am4jB7qYNMVuKcd.MyKuF.LQOZlW', NULL, '2019-01-06 11:29:40', '2019-01-06 11:29:40'),
(190009, 'ken', 'ray', 'dignos', 0, 'leyte ma boi', 'ken@email.com', NULL, '$2y$10$DkAAWPDxiYYaoL7SK9/7peq5pnNGEOtuUsXZ0C/PbTsb80t1hkPHi', NULL, '2019-01-06 11:30:07', '2019-01-06 11:30:07'),
(190010, 'dora', 'dora', 'dora', 0, 'the explorer', 'dora@email.com', NULL, '$2y$10$HBa54JiQnKL7SrKbivs/VeR4JApYE6rhEoJHPFbKw1f3VBiLTSXb6', NULL, '2019-01-06 11:30:30', '2019-01-06 11:30:30'),
(190011, 'swiper', 'no', 'swiping', 0, 'pssstttt', 'swiper@email.com', NULL, '$2y$10$YTFgXy/943knYxAD8uvuh.D8wDSxhYOHH69nHchPjcehpB2wH50tq', NULL, '2019-01-06 11:31:29', '2019-01-06 11:31:29'),
(190012, 'red', 'dead', 'redemption', 0, 'game', 'game@email.com', NULL, '$2y$10$e80S8KKPAbRM659yYSNo4uv8LVLly1aFm/qsC5nJPkD2frUx5BKxa', NULL, '2019-01-06 11:32:10', '2019-01-06 11:32:10'),
(190013, 'salazar', 'nikka', 'a', 0, 'carcar', 'nikka@email.com', NULL, '$2y$10$jdDAvDDu0IZ1ZQFHMzBze.AnxLcSl6o3JMJ8WSHID6DefIZZBCMbi', NULL, '2019-01-06 11:33:26', '2019-01-06 11:33:26'),
(190014, 'jon', 'jon', 'jon', 1, 'jon', 'jon@email.com', NULL, '$2y$10$bvu.RfyWfTyqLDoFEVmuc.c1vantu3k03e/Um/UKCZuMOcFm8IyeC', NULL, '2019-01-06 11:34:20', '2019-01-06 11:34:20'),
(190015, 'do', 'do', 'do', 1, 'do', 'do@email.com', NULL, '$2y$10$V7aW3mKVRbLYZZM0FvtCh.x3OacW7WkAICVCXVOZBiPrZYJOB9Y16', NULL, '2019-01-06 11:34:52', '2019-01-06 11:34:52'),
(190016, 're', 're', 're', 0, 're', 're@email.com', NULL, '$2y$10$rXD2qyaxU6vyFEpYhZlaHewTj4hCqegQ/qP1Dcg9NI8kFNbGjLgRa', NULL, '2019-01-06 11:35:06', '2019-01-06 11:35:06'),
(190017, 'mi', 'mi', 'mi', 1, 'mi', 'mi@email.com', NULL, '$2y$10$.v5G3SQ1RJOG6eHtzmqWGOHU/PJk/h5SeAAu203Uv9Yix0ak0YHR2', NULL, '2019-01-06 11:35:23', '2019-01-06 11:35:23'),
(190018, 'fa', 'faf', 'fa', 0, 'fa', 'fa@email.com', NULL, '$2y$10$19AVufqbpptE3OkDVyGtb./eraECHfZLywZQQWJ4cq4vIkogavp.2', NULL, '2019-01-06 11:35:51', '2019-01-06 11:35:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190019;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
