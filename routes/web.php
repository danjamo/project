<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PagesController@index');
Route::get('/login', 'PagesController@signIn');
Route::get('/terms', 'PagesController@terms');
Route::get('/about', 'PagesController@about');

// Route::get('/admin/Add_Member', 'UsersController@adminAddMember')->name('addMember');
// Route::resource('/admin/Add_Member', 'UsersController');
Route::resource('/admin', 'UsersController');

Route::post('/checklogin', 'LoginController@login')->name('login');
Route::get('/logout', 'LoginController@logout')->name('logout');

// Route::get('/test', 'LoginController@test');

// Route::post('main/addMem', 'RegisterController@add')

// Route::get('/user', 'PagesController@userIndex');
// Route::post('login/checklogin', 'PagesController@checkLogin');
// Route::get('user/logout', 'PagesController@logout');