<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * You could also do this:
         * 
         * DB::table('users')->insert([ .. ]);
         * 
         * The difference: 'created_at' and
         * 'updated_at' columns are NULL
         * 
         * unlike User::create([ .. ]);
         */
 
        // User::create([
        //     'id'=>190001,
        //     'lname'=>'jon',
        //     'fname'=>'snow',
        //     'mname'=>'abrakadaber',
        //     'email'=>'js@email.com',
        //     'password'=> Hash::make('123456'),
        //     'remember_token'=>str_random(10),
        // ]);
        // User::create([
        //     'lname'=>'momo',
        //     'fname'=>'daniel joseph',
        //     'mname'=>'jabber',
        //     'email'=>'dj@email.com',
        //     'password'=> Hash::make('123456'),
        //     'remember_token'=>str_random(10),
        // ]);
        // User::create([
        //     'lname'=>'mustang',
        //     'fname'=>'ford',
        //     'mname'=>'horsie',
        //     'email'=>'mf@email.com',
        //     'password'=> Hash::make('123456'),
        //     'remember_token'=>str_random(10),
        // ]);

        User::create([
            'lname'=>'I',
            'fname'=>'Am',
            'mname'=>'Joker',
            'email'=>'joker@email.com',
            'password'=> Hash::make('123456'),
            'remember_token'=>str_random(10),
        ]);
    }
}
