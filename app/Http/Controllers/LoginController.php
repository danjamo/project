<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        /**
         * Customize the Validation Error Message.
         * In this case, the Member ID accepting
         * only numeric values
         */
        $messages = [
            'id.required' => 'The Member ID field is required',
            'numeric' => 'Please enter a valid Member ID',
        ];

        /**
         * This will return the errors committed
         * by the user
         */
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'password' => 'required|alphaNum'
        ], $messages);

        /**
         * The submitted POST request by the user
         */
        $credentials = array(
            'id' => $request->get('id'),
            'password' => $request->get('password')
        );
        
        if (Auth::attempt($credentials)) {
            // Authentication passed...

            // This should be the dashboard
            // CHANGE THIS ASAP
            return redirect('/admin');

            // return redirect('main/admin/Add_Member');
        }

        else {
            // return back()->with('error', 'Error');
            // $data = array(
            //     'login' => 'Invalid Login Credentials',
            //     'id' => $request->get('id'),
            // );
            // return back()->withErrors($validator)->with($data);
            // return back()->with($data);
            // return redirect('login')->withErrors($validator)->with($data);
            $data = 'Invalid Login Attempt';
            return back()->withInput($request->input())->withErrors($validator)->with('error', $data);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
