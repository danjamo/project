<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * Other methods
         * $users = User::orderBy('id', 'desc')->get();
         * $users = User::where('id', 190001)->get();
         * 
         * Requires DB
         * $users = DB::select('SELECT * FROM users');
         */

        // $users = User::orderBy('id', 'desc')->take(1)->get();

        $users = User::orderBy('id', 'desc')->paginate(10);
        return view('pages.admin.manageAccounts')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('id', 'desc')->take(1)->get();
        return view('pages.admin.addMember')->with('users', $users);
        // return view('pages.admin.addMember');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'The :attribute field is required',
        ];

        $this->validate($request, [
            'lname' => 'required',
            'fname' => 'required',
            'mname' => 'required',
            'email' => 'required',
            'address' => 'required',
        ], $messages);

        $user = new User;
        $user->id = $request->input('id');
        $user->user_type = $request->input('type');
        $user->lname = $request->input('lname');
        $user->fname = $request->input('fname');
        $user->mname = $request->input('mname');
        $user->password = Hash::make('123456');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->save();

        return redirect('/admin')->with('success', 'User added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('pages.admin.userInfo')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function adminAddMember()
    {
        // return User::orderBy('id', 'desc')->take(1)->get();
        $users = User::orderBy('id', 'desc')->take(1)->get();
        return view('pages.admin.addMember')->with('users', $users);
    }
}
